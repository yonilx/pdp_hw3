#include <mpi.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define MAX_INT INT_MAX

bool even_partition_size(int vertexNum, int partition[vertexNum]);
int calc_split_price(int vertexNum, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum]);

// Creates the process'es initial partition
void initialPartition(int vertexNum, int initPar[vertexNum], int myRank) {
    initPar[0] = 0; // initial vertex 0 value (doesn't matter)

    for(int i=1; i<vertexNum; i++) {
        initPar[i] = myRank % 2;
        myRank = myRank/2;
    }
}


int graph_partition_recursive(int vertexNum, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum],
                              int numOfSetVertexes, int bestSoFar) {
    // all vertexes set
    if(vertexNum == numOfSetVertexes) {
        if(!even_partition_size(vertexNum, partition)) {
            return MAX_INT;
        }
        return calc_split_price(vertexNum, adjMatrix, partition);
    }

    // price so far is bigger or equal to the best (not gonna get better) - prunning
    if(calc_split_price(numOfSetVertexes-2, adjMatrix, partition) >= bestSoFar) //TODO : is -2?
        return MAX_INT;

    int* localBestPartition0 = (int*) malloc(vertexNum * sizeof(int));
    int* localBestPartition1 = (int*) malloc(vertexNum * sizeof(int));
    memcpy(localBestPartition0, partition, vertexNum * sizeof(int));
    memcpy(localBestPartition1, partition, vertexNum * sizeof(int));

    localBestPartition0[numOfSetVertexes] = 0;
    int localBestPrice0 = graph_partition_recursive(vertexNum, adjMatrix, localBestPartition0,
                                                    numOfSetVertexes+1, bestSoFar); // recursive call

    if(localBestPrice0 < bestSoFar) {
        bestSoFar = localBestPrice0; // local best 0 is better
        memcpy(partition, localBestPartition0, vertexNum * sizeof(int)); // copy local best to output $partition
    }


    localBestPartition1[numOfSetVertexes] = 1;
    int localBestPrice1 = graph_partition_recursive(vertexNum, adjMatrix, localBestPartition1,
                                                    numOfSetVertexes+1, bestSoFar); // recursive call

    if(localBestPrice1 < bestSoFar) {
        bestSoFar = localBestPrice1; // local best 1 is better
        memcpy(partition, localBestPartition1, vertexNum * sizeof(int)); // copy local best to output $partition
    }

    // free locals
    free(localBestPartition0);
    free(localBestPartition1);

    // $partition contains the best partition, or if $bestSoFar=-1 doesn't matter
    return bestSoFar;
}

// The static parellel algorithm main function.
int graph_partition(int vertexNum, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum]) {
    int procNum, myRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Comm_size(MPI_COMM_WORLD, &procNum);
    int** adjMat;
    if(myRank==0){
        for(int i=1;i<procNum;i++){
            MPI_Ssend(&vertexNum,1,MPI_INT,i,1,MPI_COMM_WORLD);
            MPI_Ssend(&(adjMatrix[0][0]),vertexNum*vertexNum,MPI_INT,i,2,MPI_COMM_WORLD);
        }
        adjMat =(int**)adjMatrix;
    } else {
        MPI_Recv(&vertexNum, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, NULL);
        adjMat = (int **) malloc(sizeof(int) * vertexNum * vertexNum);
        MPI_Recv(adjMat, vertexNum * vertexNum, MPI_INT, 0, 2, MPI_COMM_WORLD, NULL);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // sets partition to the initial partition
    initialPartition(vertexNum, partition, myRank);

    graph_partition_recursive(vertexNum, adjMatrix, partition, log(procNum)+1, MAX_INT);
    int bestCost = MAX_INT;
    if(myRank == 0) {
        int* localPartitions = (int*) malloc( vertexNum * procNum * sizeof(int) );
        MPI_Gather(partition, vertexNum, MPI_INT,
                   localPartitions, vertexNum, MPI_INT,
                   0, MPI_COMM_WORLD);
        for(int i=0; i<procNum; i++){
            int* threadPartition = (localPartitions + i * vertexNum);
            int cost = calc_split_price(vertexNum,adjMatrix, threadPartition);
            if (cost < bestCost && even_partition_size(vertexNum, threadPartition)) {
                bestCost = cost;
                memcpy(partition, threadPartition, vertexNum * sizeof(int));
            }
        }
        free(localPartitions);
    } else {
        free(adjMat);
        MPI_Gather(partition, vertexNum, MPI_INT,
                   NULL, vertexNum, MPI_INT,
                   0, MPI_COMM_WORLD);
    }

    return bestCost;
}


int calc_split_price(int vertexNum, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum]) {
    int cost_sum = 0;
    for(int i=0;i<vertexNum;i++){
        for(int j=0;j<vertexNum;j++) {
            if(partition[i] != partition[j]) { // on different partitions
                cost_sum += adjMatrix[i][j]; // add edge cost
            }
        }
    }
    return cost_sum;
}

bool even_partition_size(int vertexNum, int partition[vertexNum]){
    int size1 = 0, size0 = 0;
    for(int i=0;i<vertexNum;i++) {
        if(partition[i] == 0 ){
            size0++;
        }else{
            size1++;
        }
    }

    return (size0 == size1); // num of 1s is equal to 0s
}