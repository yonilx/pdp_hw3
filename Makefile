# compile the c files into o's.
graph_partition.o: graph_partition.c
	mpicc -c -std=c99 graph_partition.c

graph_partition_static.o: graph_partition_static.c
	mpicc -c -std=c99 graph_partition_static.c

main.o: main.c
	mpicc -c -std=c99 main.c

# make the executables: course example, dynamic and static.
graph_partition_236370: main.o graph_partition_236370.o
	mpicc -o graph_partition_236370 main.o graph_partition_236370.o

graph_partition: main.o graph_partition.o
	mpicc -o graph_partition main.o graph_partition.o

graph_partition_static: main.o graph_partition_static.o
	mpicc -o graph_partition_static main.o graph_partition_static.o
	
# clean previous builds
clean:
	@rm -f graph_partition.o main.o graph_partition_static.o
	@echo "Cleaned"

# run the executables: course example, dynamic and static.
# you may change the number of procs below to use more than 2.
run_236370: graph_partition_236370
	@echo "Running supplied implementation: graph_partition_236370" 
	mpirun -np 2 graph_partition_236370
	
run_graph_partition: graph_partition_static
	@echo "Running dynamic implementation: graph_partition"
	mpirun -np 2 graph_partition
	
run_graph_partition_static: graph_partition_static
	@echo "Running static implementation: graph_partition_static"
	mpirun -np 4 graph_partition_static
