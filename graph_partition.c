#include <mpi.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define MAX_INT INT_MAX
#define RECV_PARTITION_TAG 1
#define SEND_WORK_TAG 2

typedef struct {
    int numOfParts;
    int partNum;
    int bestCost;
} PartMessage;

void buildPartMessage(MPI_Datatype *message_type_ptr) {
    PartMessage indata;
    int block_len[5];
    MPI_Aint displacements[5];
    MPI_Aint addresses[6];
    MPI_Datatype t[] = {MPI_LB, MPI_INT, MPI_INT, MPI_INT, MPI_UB};

    block_len[0] = block_len[1] = block_len[2] = block_len[3] = block_len[4] = 1;
    MPI_Address((void *) &indata, &addresses[0]);
    MPI_Address((void *) &(indata.numOfParts), &addresses[1]);
    MPI_Address((void *) &(indata.partNum), &addresses[2]);
    MPI_Address((void *) &(indata.bestCost), &addresses[3]);
    displacements[0] = 0;
    displacements[1] = addresses[1] - addresses[0];
    displacements[2] = addresses[2] - addresses[0];
    displacements[3] = addresses[3] - addresses[0];
    displacements[4] = displacements[3] + sizeof(int);

    // Create the derived type
    MPI_Type_struct(5, block_len,
                    displacements, t, message_type_ptr);

    // Commit it so that it can be used
    MPI_Type_commit(message_type_ptr);
}

bool cantBeEven(int *partition, int numOfSetVertexes, int vertexNum) {
    int ones = 0, zeros = 0;
    for (int i = 0; i < numOfSetVertexes; i++) {
        if (partition[i] == 0)
            zeros++;
        else
            ones++;
    }
    return (abs(ones - zeros) > vertexNum - numOfSetVertexes);
}

int calc_split_price(int vertexNum, int vertexUntil, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum]) {
    int cost_sum = 0;
    for (int i = 0; i < vertexUntil; i++) {
        for (int j = i + 1; j < vertexUntil; j++) {
            if (partition[i] != partition[j]) { // on different partitions
                cost_sum += adjMatrix[i][j]; // add edge cost
                cost_sum += adjMatrix[j][i];
            }
        }
    }
    return cost_sum;
}

/**
 * checks if the number of 0s is equal to the number of 1s
 */
bool even_partition_size(int vertexNum, int partition[vertexNum]) {
    int size1 = 0, size0 = 0;
    for (int i = 0; i < vertexNum; i++) {
        if (partition[i] == 0) {
            size0++;
        } else {
            size1++;
        }
    }

    return (size0 == size1); // num of 1s is equal to 0s
}

/**
 * calculates the new partition weight when setting partition[newIndex] with the old weight ($firstWeight)
 */
int calc_add_weight(int vertexNum, int newIndex, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum],
                    int firstWeight) {
    int addedWeight = 0;
    for (int i = 0; i < newIndex; i++) {
        if (partition[i] != partition[newIndex]) { // not in the same partition
            addedWeight += adjMatrix[i][newIndex];
            addedWeight += adjMatrix[newIndex][i];
        }
    }
    return addedWeight + firstWeight;
}

int graph_partition_recursive(int vertexNum, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum],
                              int numOfSetVertexes, int bestSoFar, int currentWeight, int onesZerosBalance) {

    // is all vertices set
    if (vertexNum == numOfSetVertexes) {
        if (onesZerosBalance != 0) { // not balanced
            return MAX_INT;
        }
        return currentWeight;
    }

    int localBestPartition0[vertexNum]; // memory for the recursive call's solution
    int localBestPartition1[vertexNum];
    memcpy(localBestPartition0, partition, vertexNum * sizeof(int)); // copy current partition
    memcpy(localBestPartition1, partition, vertexNum * sizeof(int));


    bool do0 = true; // do first recursive call
    bool do1 = true; // do second recursive call

    if ((abs(onesZerosBalance - 1) > vertexNum - numOfSetVertexes - 1)) // if onesZerosBalance will never be 0
        do0 = false; // don't do first recursion
    if ((abs(onesZerosBalance + 1) > vertexNum - numOfSetVertexes - 1)) // if onesZerosBalance will never be 0
        do1 = false; // don't do second recursion

    // new current weight when setting vertex[numOfSetVertexes] = 1/0
    int sum1 = currentWeight, sum0 = currentWeight;
    for (int i = 0; i < numOfSetVertexes; i++) { // add weight to sums
        if (partition[i] == 1) {
            sum0 += adjMatrix[i][numOfSetVertexes];
            sum0 += adjMatrix[numOfSetVertexes][i];
        } else {
            sum1 += adjMatrix[i][numOfSetVertexes];
            sum1 += adjMatrix[numOfSetVertexes][i];
        }
    }

    int localBestPrice0 = MAX_INT;
    int localBestPrice1 = MAX_INT;
    localBestPartition0[numOfSetVertexes] = 0;
    if (do0) if (sum0 >= bestSoFar) // price so far is bigger or equal to the best (not gonna get better) - prunning
        localBestPrice0 = MAX_INT;
    else
        localBestPrice0 = graph_partition_recursive(vertexNum, adjMatrix, localBestPartition0,
                                                    numOfSetVertexes + 1, bestSoFar, sum0,
                                                    (onesZerosBalance - 1)); // recursive call

    if (localBestPrice0 < bestSoFar) {
        bestSoFar = localBestPrice0; // local best 0 is better
        memcpy(partition, localBestPartition0, vertexNum * sizeof(int)); // copy local best to output $partition
    }


    localBestPartition1[numOfSetVertexes] = 1;
    if (do1) if (sum1 >= bestSoFar) // price so far is bigger or equal to the best (not gonna get better) - prunning
        localBestPrice1 = MAX_INT;
    else
        localBestPrice1 = graph_partition_recursive(vertexNum, adjMatrix, localBestPartition1,
                                                    numOfSetVertexes + 1, bestSoFar, sum1,
                                                    (onesZerosBalance + 1)); // recursive call

    if (localBestPrice1 < bestSoFar) {
        bestSoFar = localBestPrice1; // local best 1 is better
        memcpy(partition, localBestPartition1, vertexNum * sizeof(int)); // copy local best to output $partition
    }

    // $partition contains the best partition
    return bestSoFar;
}

// Creates the process'es work partition
void workPartition(int vertexNum, int partition[vertexNum], int partNum) {
    partition[0] = 0; // initial vertex 0 value (doesn't matter)
    for (int i = 1; i < vertexNum; i++) {
        partition[i] = partNum % 2;
        partNum = partNum / 2;
    }
}

/*
 * helper function to help us send one long instead of a int array
 * converts a binary int array into a long
 */
long int array2long(int vertexNum, int *array) {
    long int res = 0;
    int mult = 1;
    for (int i = 0; i < vertexNum; i++) {
        res += mult * array[i];
        mult *= 2;
    }
    return res;
}

/*
 * helper function to help us send one long instead of a int array
 * converts a converted long (using array2long) to binary int array
 */
void long2array(long int res, int vertexNum, int array[vertexNum]) {
    for (int i = 0; i < vertexNum; i++) {
        array[i] = res % 2;
        res /= 2;
    }
    return;
}


// The dynamic parallel algorithm main function.
int graph_partition(int vertexNum, int adjMatrix[vertexNum][vertexNum], int partition[vertexNum]) {
    int procNum, myRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank); // get process'es rank
    MPI_Comm_size(MPI_COMM_WORLD, &procNum); // get number of processes

    MPI_Datatype message_type;
    buildPartMessage(&message_type); // initialize the PartMessage datatype

    MPI_Request request;
    MPI_Status status;
    if (myRank == 0) {
        int bestCost = MAX_INT;
        // workerPartition[0] = the partition converted into a long
        // workerPartition[1] = partition's cost
        long int *workerPartition = (long int *) malloc(2 * sizeof(long int));
        int numOfParts = 2 * (procNum - 1); //TODO: change me maybe
        long int bestPartition_converted; // the best partition so far, conveted using array2long
        for (int i = 0; i < numOfParts; i++) {
            PartMessage msg;
            msg.numOfParts = numOfParts;
            msg.partNum = i;
            msg.bestCost = bestCost;
            MPI_Recv(workerPartition, 2, MPI_LONG, MPI_ANY_SOURCE, RECV_PARTITION_TAG, MPI_COMM_WORLD, &status);
            MPI_Ibsend(&msg, 1, message_type, status.MPI_SOURCE, SEND_WORK_TAG, MPI_COMM_WORLD, &request);
            if (bestCost > workerPartition[1]) { // is cost better
                bestCost = workerPartition[1];
                bestPartition_converted = workerPartition[0];
            }
        }
        for (int i = 1; i < procNum; i++) {
            MPI_Recv(workerPartition, 2, MPI_LONG, MPI_ANY_SOURCE, RECV_PARTITION_TAG, MPI_COMM_WORLD, &status);
            if (bestCost > workerPartition[1]) { // is cost better
                bestCost = workerPartition[1];
                bestPartition_converted = workerPartition[0];
            }
            PartMessage msg;
            msg.numOfParts = -1; // KILL signal
            msg.partNum = 0;
            msg.bestCost = MAX_INT;
            MPI_Ibsend(&msg, 1, message_type, status.MPI_SOURCE, SEND_WORK_TAG, MPI_COMM_WORLD, &request);
        }
        free(workerPartition); // free mallocs

        int a = 0; // dummy value for broadcast
        MPI_Bcast(&a, 1, MPI_INT, 0, MPI_COMM_WORLD);
        long2array(bestPartition_converted, vertexNum, partition); // convert back to
        return bestCost;
    }
    else { //worker
        int workerPartition[vertexNum]; // the unconverted partition the worker works on
        int bestLocalCost = MAX_INT;
        PartMessage msg;

        long int send[2];
        send[0] = array2long(vertexNum, workerPartition);
        send[1] = MAX_INT;
        MPI_Ibsend(send, 2, MPI_LONG, 0, RECV_PARTITION_TAG, MPI_COMM_WORLD, &request);//send dummy result
        MPI_Recv(&msg, 1, message_type, 0, SEND_WORK_TAG, MPI_COMM_WORLD, NULL);
        while (msg.numOfParts != -1) { // not termination msg
            workPartition(vertexNum, workerPartition, msg.partNum); // build work partition

            int weight = calc_split_price(vertexNum, log(msg.numOfParts) + 1, adjMatrix, workerPartition);
            int onesZerosBalance = 0;
            for (int i = 0; i < log(msg.numOfParts); i++) {
                if (workerPartition[i] == 0)
                    onesZerosBalance--;
                else
                    onesZerosBalance++;
            }
            bestLocalCost = graph_partition_recursive(vertexNum, adjMatrix, workerPartition, log(msg.numOfParts) + 1,
                                                      msg.bestCost, weight, onesZerosBalance);

            //workerPartition[vertexNum + 1] = bestLocalCost;
            //workerPartition[vertexNum]=myRank;
            send[0] = array2long(vertexNum, workerPartition);
            send[1] = bestLocalCost;
            // send work

            MPI_Ibsend(send, 2, MPI_LONG, 0, RECV_PARTITION_TAG, MPI_COMM_WORLD, &request);

            // wait for new work
            MPI_Recv(&msg, 1, message_type, 0, SEND_WORK_TAG, MPI_COMM_WORLD, NULL);

        }
        int a = 0;
        MPI_Bcast(&a, 1, MPI_INT, 0, MPI_COMM_WORLD); //wait for KILL signal
        return 0;
    }
}
